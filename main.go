package main

import (
	"fmt"

	"gitlab.com/celduin/golang-experimental-code/fibonacci"
)

func main() {
	for {
		choice := printMenu()
		cont := executeSelection(choice)
		if cont == false {
			break
		}
	}
}

func printMenu() (choice string) {
	//Function to print the menu options
	fmt.Println("Please choose from one of the following options:")
	fmt.Println("A - Display help")
	fmt.Println("B - Fibonacci Sequence")
	fmt.Println("Exit - Exit the application")
	fmt.Scanln(&choice)
	return choice
}

func executeSelection(choice string) (cont bool) {
	cont = true
	switch choice {
	case "A",
		"a":
		printHelp()
		break
	case "B",
		"b":
		fibonacci.Start()
		break
	case "Exit",
		"exit":
		cont = false
		break
	}
	return cont
}

func printHelp() {
	fibonacci.Help()
}
